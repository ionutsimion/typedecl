# typedecl
C++ strong types, inspired by:
* Jonathan Boccara's series on strong types (https://www.fluentcpp.com/2016/12/08/strong-types-for-strong-interfaces/)
* Pierre Baillargeon's blog post: "Hypotetical C++: easy type creation" (https://www.spiria.com/en/blog/desktop-software/hypothetical-c-easy-type-creation/)

# typelist
Type traits and manipulation (to some degree) inspired by Andrei Alexandrescu's "Modern C++ Design" (http://erdani.org/index.php/books/modern-c-design/index.html)
